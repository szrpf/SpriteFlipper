import SpriteFlipper from "./SpriteFlipper";

const { ccclass } = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {
    坤坤: SpriteFlipper = null;
    樱木: SpriteFlipper = null;
    地图: SpriteFlipper = null;
    angle: number = 0;
    start() {
        this.坤坤 = this.node.getChildByName('坤坤').getComponent(SpriteFlipper);
        this.樱木 = this.node.getChildByName('樱木').getComponent(SpriteFlipper);
        this.地图 = this.node.getChildByName('地图').getComponent(SpriteFlipper);
        this.schedule(() => {
            this.angle++;
            this.坤坤.angle = -this.angle;
            this.樱木.angle = this.angle;
            this.地图.angle = this.angle;
        }, 0.01);
    }
}






